#!/bin/bash
apt install terminator -Y
apt install perl -y
apt install libimage-exiftool-perl -y
apt install binwalk -y
apt-get install john -y
pip3 install requests
pip3 install cryptography
pip3 install pwntools
pip3 install cyclic
apt-get install binutils -y
apt-get install nmap
pip3 install wfuzz
apt-get install zip unzip
apt install wget
sudo snap install tesseract --edge
$(
cd /tmp
curl https://raw.githubusercontent.com/rapid7/metasploit-omnibus/master/config/templates/metasploit-framework-wrappers/msfupdate.erb > msfinstall
chmod +x msfinstall
./msfinstall)

mkdir /opt/tools
wget https://ghidra-sre.org/ghidra_9.2.3_PUBLIC_20210325.zip -O /opt/tools/ghidra_9.2.3_PUBLIC_20210325.zip && unzip /opt/tools/ghidra_9.2.3_PUBLIC_20210325.zip && mv /opt/tools/ghidra_9.2.3_PUBLIC /opt/tools/ghidra/
rm /opt/tools/ghidra_9.2.3_PUBLIC_20210325.zip
echo "PATH=/opt/tools/ghidra/:$PATH" >> ~/.bashrc
google-chrome --app='https://portswigger.net/burp/releases/professional-community-2021-5-1'
