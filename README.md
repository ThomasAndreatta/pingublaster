# MASTER BRANCH - PINGU BLASTER
## Purpose
This repo contains all my personal tools and script - and some configuration file - for hacking automatization and recreation of my personal machine setup. 
-----

## File Content
* bashrc - PinguBlaster:\
Add the content of the "bashrc - PinguBlaster" file in your ~/.bashrc file, modify the content of the alias and export command and you'll be ready to go

* base_program.sh:\
A script which automatically install all the basic tools needed on an hacking machine


* terminator.config:\
Contains the configuration for terminator terminal, copy the content into ~/.config/terminator/config

-----
###  TO ADD - base_program.sh
#### CLI
* nmap: host port-scanning
* john the ripper: hash-cracking
* hashcat: hash-cracking
* cewl: dictionary creation
* metasploit: exploit framework
* msfvenom: payload generator
* binwalk: extracting firmware from images
* tesseract: extract text from images
* exiftool: file metadata manipulation
* hydra: remote login-bruteforce
* gdb: decompiler and debugger
* openvpn: vpn software
#### GUI
* terminator: terminal under steroids
* atom: IDE
* vscode: IDE
* sublime: text editor
* wireshark: network packet manipulation
* obsidian:
* discord: chat with people
* telegram: chat with people
* chrome: browser
* ghidra: decompiler and debugger
* burp suite: proxy/internet packet analyzer and manipulation
